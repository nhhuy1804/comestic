﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComesticStore.Models.ViewModel
{
    public class CategoryViewModel
    {
        public long ID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryParentID { get; set; }
        public string CategoryParentName { get; set; }
        public string Description { get; set; }
        public string ImageUpload { get; set; }
        public bool Status { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
    }
}