﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComesticStore.Models.ViewModel
{
    public class SupplierViewModel
    {
        public long ID { get; set; }
        public string SupplierName { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
    }
}