﻿using System;
using System.IO;

namespace ComesticStore
{
    public class LogWriter
    {
        public string path = System.Web.HttpContext.Current.Server.MapPath("~\\Log");
        public void LogWrite(string logMess)
        {
            try
            {
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                using (StreamWriter w = File.AppendText(path + "\\log-"+ DateTime.Now.ToString("dd-MM-yyyy") +".txt"))
                {
                    Log(logMess, w);
                }
            }
            catch
            {

            }
        }
        public void Log(string logMess, TextWriter txt)
        {
            try
            {
                txt.Write("\r\nLog Entry: ");
                txt.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                txt.WriteLine(" :");
                txt.WriteLine(" :{0}", logMess);
                txt.WriteLine("----------------------------------");
            }
            catch { }
        }
    }
}