﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ComesticStore.Models;
using ComesticStore.Models.ViewModel;
using ComesticStore.Services;

namespace ComesticStore.Api
{
    [RoutePrefix("api/Category")]
    public class CategoryController : ApiControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        
        [HttpPost]
        [Route("AddorUpdateCategory")]
        public HttpResponseMessage AddorUpdateCategory(HttpRequestMessage request, CategoryViewModel data)
        {
            return CreateHttpResponse(request, () =>
            {
                string picture = "";
                if (data.ImageUpload != "" && data.ImageUpload != null)
                {
                    picture = "/Upload/Category/" + DateTime.Now.Ticks.ToString() + ".png";
                    Utils.UploadImage(data.ImageUpload, picture);
                }
                Category model = new Category
                {
                    CategoryID = data.Status ? 0: data.ID,
                    Active = true,
                    CategoryName = data.CategoryName,
                    CategoryParentID = data.CategoryParentID,
                    DateCreate = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Description = data.Description,
                    Picture = picture
                };
                bool result = _categoryService.AddorUpdateCategory(model);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { result });
                return response;
            });
        }

        [HttpGet]
        [Route("GetCategorys")]
        public HttpResponseMessage GetCategorys(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var data = _categoryService.GetCategorys();
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { data });
                return response;
            });
        }

        [HttpGet]
        [Route("GetCategory")]
        public HttpResponseMessage GetCategory(HttpRequestMessage request, long ID)
        {
            return CreateHttpResponse(request, () =>
            {
                var data = _categoryService.GetCategory(ID);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { data });
                return response;
            });
        }

        [HttpPost]
        [Route("RemoveCategorys")]
        public HttpResponseMessage RemoveCategorys(HttpRequestMessage request, string IDs)
        {
            return CreateHttpResponse(request, () =>
            {
                bool result = _categoryService.RemoveCategorys(IDs);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { result });
                return response;
            });
        }
    }
}
