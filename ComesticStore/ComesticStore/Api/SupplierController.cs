﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ComesticStore.Models;
using ComesticStore.Models.ViewModel;
using ComesticStore.Services;

namespace ComesticStore.Api
{
    [RoutePrefix("api/Supplier")]
    public class SupplierController : ApiControllerBase
    {
        private readonly ISupplierService _supplierService;
        public SupplierController(ISupplierService supplierService)
        {
            _supplierService = supplierService;
        }

        [HttpPost]
        [Route("AddorUpdateSupplier")]
        public HttpResponseMessage AddorUpdateSupplier(HttpRequestMessage request, SupplierViewModel data)
        {
            return CreateHttpResponse(request, () =>
            {
                Supplier model = new Supplier
                {
                    Active = true,
                    SupplierName = data.SupplierName,
                    DateCreate = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Description = data.Description
                };
                bool result = _supplierService.AddorUpdateSupplier(model);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { result });
                return response;
            });
        }

        [HttpGet]
        [Route("GetSuppliers")]
        public HttpResponseMessage GetSuppliers(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var data = _supplierService.GetSuppliers();
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { data });
                return response;
            });
        }
    }
}
