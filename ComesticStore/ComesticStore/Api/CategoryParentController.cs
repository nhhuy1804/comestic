﻿using ComesticStore.Models;
using ComesticStore.Models.ViewModel;
using ComesticStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ComesticStore.Api
{
    [RoutePrefix("api/CategoryParent")]
    public class CategoryParentController : ApiControllerBase
    {
        private readonly ICategoryParentService _categoryParentService;
        public CategoryParentController(ICategoryParentService categoryParentService)
        {
            _categoryParentService = categoryParentService;
        }

        [HttpPost]
        [Route("AddorUpdateCategoryParent")]
        public HttpResponseMessage AddorUpdateCategoryParent(HttpRequestMessage request, CategoryParentViewModel data)
        {
            return CreateHttpResponse(request, () =>
            {
                CategoryParent model = new CategoryParent
                {
                    Active = true,
                    CategoryParentName = data.CategoryParentName,
                    DateCreate = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Description = data.Description,
                    CategoryParentType = data.CategoryParentType,
                    CategoryParentID = data.ID
                };
                bool result = _categoryParentService.AddorUpdateCategoryParent(model);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { result });
                return response;
            });
        }

        [HttpGet]
        [Route("GetCategoryParents")]
        public HttpResponseMessage GetCategoryParents(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var data = _categoryParentService.GetCategoryParents();
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { data });
                return response;
            });
        }

        [HttpGet]
        [Route("GetCategoryParent")]
        public HttpResponseMessage GetCategoryParent(HttpRequestMessage request, long ID)
        {
            return CreateHttpResponse(request, () =>
            {
                var data = _categoryParentService.GetCategoryParent(ID);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { data });
                return response;
            });
        }

        [HttpPost]
        [Route("RemoveCategoryParents")]
        public HttpResponseMessage RemoveCategoryParents(HttpRequestMessage request, string IDs)
        {
            return CreateHttpResponse(request, () =>
            {
                bool result = _categoryParentService.RemoveCategoryParents(IDs);
                HttpResponseMessage response = request.CreateResponse(System.Net.HttpStatusCode.OK, new { result });
                return response;
            });
        }
    }
}