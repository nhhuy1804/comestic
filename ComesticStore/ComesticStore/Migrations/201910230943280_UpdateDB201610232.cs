namespace ComesticStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDB201610232 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categorys", "DateCreate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Categorys", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Categorys", "Active", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "DateCreate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "Active", c => c.Boolean(nullable: false));
            AddColumn("dbo.Suppliers", "DateCreate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Suppliers", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Suppliers", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "Active");
            DropColumn("dbo.Suppliers", "DateUpdate");
            DropColumn("dbo.Suppliers", "DateCreate");
            DropColumn("dbo.Products", "Active");
            DropColumn("dbo.Products", "DateUpdate");
            DropColumn("dbo.Products", "DateCreate");
            DropColumn("dbo.Categorys", "Active");
            DropColumn("dbo.Categorys", "DateUpdate");
            DropColumn("dbo.Categorys", "DateCreate");
        }
    }
}
