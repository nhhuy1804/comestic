namespace ComesticStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDB201910243 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoryParents", "CategoryParentType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CategoryParents", "CategoryParentType");
        }
    }
}
