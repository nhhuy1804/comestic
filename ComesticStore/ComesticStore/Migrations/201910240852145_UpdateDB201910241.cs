namespace ComesticStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDB201910241 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "CategoryID", "dbo.Categorys");
            DropForeignKey("dbo.Products", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.Products", new[] { "CategoryID" });
            DropIndex("dbo.Products", new[] { "SupplierID" });
            AddColumn("dbo.Categorys", "CategoryParentID", c => c.Long(nullable: false));
            AddColumn("dbo.Suppliers", "Description", c => c.String());
            DropColumn("dbo.Categorys", "CategoryType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Categorys", "CategoryType", c => c.String());
            DropColumn("dbo.Suppliers", "Description");
            DropColumn("dbo.Categorys", "CategoryParentID");
            CreateIndex("dbo.Products", "SupplierID");
            CreateIndex("dbo.Products", "CategoryID");
            AddForeignKey("dbo.Products", "SupplierID", "dbo.Suppliers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Products", "CategoryID", "dbo.Categorys", "CategoryID", cascadeDelete: true);
        }
    }
}
