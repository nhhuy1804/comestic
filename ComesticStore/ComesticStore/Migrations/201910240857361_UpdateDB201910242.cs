namespace ComesticStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDB201910242 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryParents",
                c => new
                    {
                        CategoryParentID = c.Long(nullable: false, identity: true),
                        CategoryParentName = c.String(),
                        Description = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryParentID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CategoryParents");
        }
    }
}
