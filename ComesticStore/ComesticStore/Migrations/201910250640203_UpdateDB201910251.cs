namespace ComesticStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDB201910251 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categorys", "CategoryParentID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categorys", "CategoryParentID", c => c.Long(nullable: false));
        }
    }
}
