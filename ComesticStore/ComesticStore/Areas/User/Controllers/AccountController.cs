﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComesticStore.Areas.User.Controllers
{
    public class AccountController : Controller
    {
        // GET: User/Account
        public ActionResult Register()
        {
            return View();
        }

        public ActionResult UserInfo()
        {
            return View();
        }

        public ActionResult WishList()
        {
            return View();
        }
    }
}