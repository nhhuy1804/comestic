﻿using ComesticStore.Models;
using ComesticStore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComesticStore.Services
{
    public interface ISupplierService
    {
        bool AddorUpdateSupplier(Supplier data);
        IEnumerable<SupplierViewModel> GetSuppliers();
    }
    public class SupplierService : ISupplierService
    {
        LogWriter log = new LogWriter();
        private readonly StoreDbContext _db;

        public SupplierService(IDbFactory dbFactory)
        {
            _db = dbFactory.Init();
        }

        public bool AddorUpdateSupplier(Supplier data)
        {
            try
            {
                var sup = _db.Suppliers.Where(x => x.ID == data.ID).FirstOrDefault();
                if (sup == null)
                {
                    _db.Suppliers.Add(data);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    _db.Suppliers.Attach(sup);
                    sup.SupplierName = data.SupplierName;
                    sup.Description = data.Description;
                    sup.DateUpdate = data.DateUpdate;
                    _db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.LogWrite(ex.Message);
                return false;
            }
        }

        public IEnumerable<SupplierViewModel> GetSuppliers()
        {
            var result = _db.Suppliers.Where(x => x.Active == true).ToList().Select(x => new SupplierViewModel
            {
                SupplierName = x.SupplierName,
                Description = x.Description,
                ID = x.ID,
                DateCreate = x.DateCreate.ToShortDateString(),
                DateUpdate = x.DateUpdate.ToShortDateString()
            }).ToList();
            
            return result;
        }
    }
}
