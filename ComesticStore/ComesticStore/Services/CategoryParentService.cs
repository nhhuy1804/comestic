﻿using ComesticStore.Models;
using ComesticStore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComesticStore.Services
{
    public interface ICategoryParentService
    {
        bool AddorUpdateCategoryParent(CategoryParent data);
        IEnumerable<CategoryParentViewModel> GetCategoryParents();
        CategoryParent GetCategoryParent(long ID);
        bool RemoveCategoryParents(string IDs);
    }
    public class CategoryParentService : ICategoryParentService
    {
        LogWriter log = new LogWriter();
        private readonly StoreDbContext _db;

        public CategoryParentService(IDbFactory dbFactory)
        {
            _db = dbFactory.Init();
        }

        public bool AddorUpdateCategoryParent(CategoryParent data)
        {
            try
            {
                var cat = _db.CategoryParents.Where(x => x.CategoryParentID == data.CategoryParentID && x.Active == true).FirstOrDefault();
                if (cat == null)
                {
                    _db.CategoryParents.Add(data);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    _db.CategoryParents.Attach(cat);
                    cat.CategoryParentName = data.CategoryParentName;
                    cat.Description = data.Description;
                    cat.DateUpdate = data.DateUpdate;
                    cat.CategoryParentType = data.CategoryParentType;
                    _db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.LogWrite(ex.Message);
                return false;
            }
        }

        public IEnumerable<CategoryParentViewModel> GetCategoryParents()
        {
            return _db.CategoryParents.Where(x => x.Active == true).ToList().Select(x => new CategoryParentViewModel
            {
                ID = x.CategoryParentID,
                CategoryParentName = x.CategoryParentName,
                Description = x.Description,
                CategoryParentType = x.CategoryParentType,
                DateCreate = x.DateCreate.ToShortDateString(),
                DateUpdate = x.DateUpdate.ToShortDateString()
            });
        }

        public CategoryParent GetCategoryParent(long ID)
        {
            return _db.CategoryParents.Where(x => x.CategoryParentID == ID && x.Active == true).FirstOrDefault();
        }

        public bool RemoveCategoryParents(string IDs)
        {
            try
            {
                long[] arr = Utils.ConvertStringToArr(IDs);

                var datapa = _db.CategoryParents.Where(f => arr.Contains(f.CategoryParentID)).ToList();
                datapa.ForEach(a => a.Active = false);

                //var data = _db.Categorys.Where(f => arr.Contains(f.CategoryParentID)).ToList();
                //data.ForEach(a => a.Active = false);
                
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                log.LogWrite(ex.Message);
                return false;
            }
        }
    }
}