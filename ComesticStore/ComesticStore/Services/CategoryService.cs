﻿using ComesticStore.Models;
using ComesticStore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComesticStore.Services
{
    public interface ICategoryService
    {
        bool AddorUpdateCategory(Category data);
        IEnumerable<CategoryViewModel> GetCategorys();
        Category GetCategory(long ID);
        bool RemoveCategorys(string IDs);
    }
    public class CategoryService : ICategoryService
    {
        LogWriter log = new LogWriter();
        private readonly StoreDbContext _db;

        public CategoryService(IDbFactory dbFactory)
        {
            _db = dbFactory.Init();
        }

        public bool AddorUpdateCategory(Category data)
        {
            try
            {
                var cat = _db.Categorys.Where(x => x.CategoryID == data.CategoryID).FirstOrDefault();
                if (cat == null)
                {
                    _db.Categorys.Add(data);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    _db.Categorys.Attach(cat);
                    cat.CategoryName = data.CategoryName;
                    cat.CategoryParentID = data.CategoryParentID;
                    cat.Description = data.Description;
                    cat.Picture = data.Picture;
                    cat.DateUpdate = data.DateUpdate;
                    _db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.LogWrite(ex.Message);
                return false;
            }
        }

        public IEnumerable<CategoryViewModel> GetCategorys()
        {
            long[] arr;

            var result = _db.Categorys.Where(x => x.Active == true).ToList().Select(x => new CategoryViewModel
            {
                CategoryName = x.CategoryName,
                CategoryParentID = x.CategoryParentID,
                Description = x.Description,
                ID = x.CategoryID,
                DateCreate = x.DateCreate.ToShortDateString(),
                DateUpdate = x.DateUpdate.ToShortDateString()
            }).ToList();

            result.ForEach(x =>
            {
                arr = Utils.ConvertStringToArr(x.CategoryParentID);
                x.CategoryParentName = string.Join(", ", _db.CategoryParents.Where(y => arr.Contains(y.CategoryParentID)).Select(y => y.CategoryParentName).ToList());
            });

            return result;
        }

        public Category GetCategory(long ID)
        {
            return _db.Categorys.Where(x => x.CategoryID == ID).FirstOrDefault();
        }

        public bool RemoveCategorys(string IDs)
        {
            try
            {
                long[] arr = Utils.ConvertStringToArr(IDs);

                var data = _db.Categorys.Where(f => arr.Contains(f.CategoryID)).ToList();
                data.ForEach(a => a.Active = false);
                
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                log.LogWrite(ex.Message);
                return false;
            }
        }
    }
}
