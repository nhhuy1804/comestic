﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ComesticStore
{
    public class Utils
    {
        //Category type
        public static string Comestic = "1";
        public static string Fashion = "2";

        //Save file by base64String
        public static void UploadImage(string base64String, string filePath)
        {
            var base64Data = Regex.Match(base64String, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            byte[] imageBytes = Convert.FromBase64String(base64Data);
            string ServerPath = HttpContext.Current.Server.MapPath("~/");
            using (var imageFile = new FileStream(ServerPath + filePath, FileMode.Create))
            {
                imageFile.Write(imageBytes, 0, imageBytes.Length);
                imageFile.Flush();
            }
        }

        public static long[] ConvertStringToArr(string myString)
        {
            try
            {
                return myString.Split(',').Select(n => Convert.ToInt64(n)).ToArray();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}