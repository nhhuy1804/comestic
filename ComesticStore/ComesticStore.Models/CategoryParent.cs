﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ComesticStore.Models
{
    [Table("CategoryParents")]
    public class CategoryParent : ModelBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CategoryParentID { get; set; }
        public string CategoryParentName { get; set; }
        public string CategoryParentType { get; set; }
        public string Description { get; set; }
    }
}
