﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComesticStore.Models
{
    public class ModelBase
    {
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdate { get; set; }
        public bool Active { get; set; }
    }
}
