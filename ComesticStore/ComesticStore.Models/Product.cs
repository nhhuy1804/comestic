﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ComesticStore.Models
{
    [Table("Products")]
    public class Product : ModelBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public long CategoryID { get; set; }
        public long SupplierID { get; set; }
        public long QuantityPerUnit { get; set; }
        public long UnitPrice { get; set; }
        public long UnitInStock { get; set; }
        public long UnitOnOrder { get; set; }
        public string Picture { get; set; }
        public string ProductDetail { get; set; }
    }
}
