﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ComesticStore.Models
{
    [Table("Categorys")]
    public class Category : ModelBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryParentID { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
    }
}
